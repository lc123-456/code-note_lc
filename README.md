用于日常生活的分享，工作的记录，用于文章写作过程中，经常使用图片插入功能，每次需要单张图片逐个插入，费时费力。通过在文章中拆入图片时自动将图片上传到图床，文章中只插入图片链接的方式，可以实现任何平台发布文章时都可以正确显示，提高了效率。
